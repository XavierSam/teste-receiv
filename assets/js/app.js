var app = {

	init: function() {
		this.mod_inputs.init();
		this.mod_cadastro.init();
	},
	mod_inputs: {

		init: function() {

			$("#document").mask('00.000.000/0000-00', {
				onKeyPress: function (cpf, ev, el, op) {
					var masks = ['000.000.000-000', '00.000.000/0000-00'];
					$("#document").mask((cpf.length > 14 || cpf.length == 0) ? masks[1] : masks[0], op);
				}
			});

			$('#amount').mask('000.000.000.000.000,00', {reverse: true});

		}
	},
	mod_cadastro: {

		init: function() {
			var $form = $("#form-invoice");
			var $url_ajax = "http://localhost/teste-receiv/";

			$("#btn-newregister").click(function(el){
					app.clearForm($form);

					$("#novoCadastro").modal("show");
			});

			$(".edit-invoice").click(function(el){
					app.clearForm($form);

					var id_invoice = $(this).data("invoice");

					$.ajax({
						type: "POST",
						url: $url_ajax,
						data: {"action": "edit", "id_invoice": id_invoice},
						dataType: 'json',
						success: function(resposta){
							if (resposta) {	
								$("#id_invoice").val(resposta.id);
								$("#name").val(resposta.name);
								$("#document").val(resposta.document);
								$("#birthdate").val(resposta.birthdate);
								$("#address").val(resposta.address);
								$("#amount").val(resposta.amount);
								$("#description").val(resposta.description);
								$("#duedate").val(resposta.duedate);
							}
						}
					});

					$("#novoCadastro").modal("show");
			});

			$(".delete-invoice").click(function(el){
					app.clearForm($form);

					var id_invoice = $(this).data("invoice");
					swal({
						title: "",
						text: "Confirmar exclusão da cobrança?",
						type: "warning",
						showCancelButton: true,
						confirmButtonClass: "btn-warning btn-round btn-fill",
						confirmButtonText: "Confirmar",
						cancelButtonText: "Cancelar",
						cancelButtonClass: "btn-default btn-round btn-fill",
						closeOnConfirm: false,
						showLoaderOnConfirm: true
					}, function() {
						$.ajax({
							type: "POST",
							url: $url_ajax,
							data: {"action": "delete", "id_invoice": id_invoice},
							dataType: 'json',
							success: function(resposta){
								if (resposta.results > 0) {	
									location.reload();
								}
							}
						});
					});
			});

		}

	},
	parseData(form) {
		var data = form.serializeArray();
		var params = {};

		$.each(data, function( index, value ) {
			params[value.name] = value.value;
		});

		return params;
	},
	clearForm: function(form){

		var $input = form.find('input:not(.btn)');
		var $select = form.find('select');
		var $textarea = form.find('textarea');

		if(typeof $input !== 'undefined'){
			$.each($input,function() {
				$(this)
				.removeClass('error, valid')
				.val("");
			});
		}

		$.each($select,function() {
			$(this)
			.removeClass('error, valid')
			.val("");
		});

		if(typeof $textarea !== 'undefined'){
			$textarea
			.removeClass('error, valid')
			.val("");
		}
	}

}

var $ = jQuery.noConflict();

(function() {
	app.init();
})();
