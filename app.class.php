<?php 

/**
* Controller Class
* 
* @author samuelxavi@gmail.com
* 
*/
class App
{

    private $db;
    public $success_message;
    public $error_message;

    /**
     * Popula parâmetros iniciais da classe
     * 
     * @param void
     * @return void
     */
    public function __construct()
    {
        session_start();
        $this->db = new Crud();
        if (!empty($_SESSION['error'])) {
            $this->error_message = $_SESSION['error'];
            unset($_SESSION['error']);
        }
        if (!empty($_SESSION['success'])) {
            $this->success_message = $_SESSION['success'];
            unset($_SESSION['success']);
        }
    }

    /**
     * Processa requisições GET para a APP
     * 
     * @param array $input
     * @return array
     */
    public function getRequest($input) {

        if (empty($input)) {
            $result = $this->db->fetchAll("invoices");

            $invoices = [];
            foreach($result as $row) {
                $row['document'] = $this->parse_doc($row['document']);

                $duedate = new DateTime($row['duedate']);
                $row['duedate'] = $duedate->format("d/m/Y");

                $birthdate = new DateTime($row['birthdate']);
                $row['birthdate'] = $birthdate->format("d/m/Y");

                $row['amount'] = number_format($row['amount'], 2, ',', '.');

                $invoices[] = $row;
            }


            return $invoices;

        }

    }


    /**
     * Processa requisições POST para a APP
     * 
     * Após o processamento o usuário é redirecionado para evitar 
     * duplicações ao atualizar a página
     * 
     * @param array $input
     * @return void
     */
    public function postRequest($input) {

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            if (!empty($input['id_invoice']) && $input['action'] === "edit") {
                $result = $this->db->fetchRow("invoices", $input['id_invoice']);
                $result['document'] = $this->parse_doc($result['document']);

                echo json_encode($result);
                die();
            } elseif (!empty($input['id_invoice']) && $input['action'] === "delete") {
                $delete = $this->db->delete("invoices", $input['id_invoice']);
                if ($delete) {
                    $_SESSION['success'] = "Registro excluído com sucesso";
                } else {
                    $_SESSION['error'] = "Houve um erro na ação";
                }

                echo json_encode(array("results" => $delete));
                die();
            }

        } else {
            if (!empty($input['document'])) {
                $input['document'] = preg_replace("/[^0-9]/", "", $input['document']);
            }
            if (!empty($input['id'])) {
                $id_invoice = $input['id'];
                unset($input['id']);
                $update = $this->db->update("invoices", $input, $id_invoice);
                if ($update) {
                    $_SESSION['success'] = "Registro atualizado com sucesso";
                } else {
                    $_SESSION['error'] = "Houve um erro na ação";
                }
            } else {
                $insert = $this->db->insert("invoices", $input);
                if ($insert) {
                    $_SESSION['success'] = "Registro inserido com sucesso";
                } else {
                    $_SESSION['error'] = "Houve um erro na ação";
                }
            }

            header("Location: " . HOME_URL);
            die();
        }
    }

    /**
     * Formata o número de CPF/CNPJ com pontuações
     * 
     * @param string $doc
     * @return string
     */
    private function parse_doc($doc) {

        $doc = preg_replace("/[^0-9]/", "", $doc);
        $len = strlen($doc);
        if ($len < 11) {
            $doc = str_pad($doc, 11, "0", STR_PAD_LEFT);
        }

        if($len > 11 ) {

            $docFormatado = substr($doc, 0, 2) . '.' .
            substr($doc, 2, 3) . '.' .
            substr($doc, 5, 3) . '/' .
            substr($doc, 8, 4) . '-' .
            substr($doc, -2);

        } else {

            $docFormatado = substr($doc, 0, 3) . '.' .
            substr($doc, 3, 3) . '.' .
            substr($doc, 6, 3) . '-' .
            substr($doc, 9, 2);

        }

        return $docFormatado;
    }
}