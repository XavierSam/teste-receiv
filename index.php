<?php 
  
  // Our configuration params
  require_once "config.php";

  // Our database class
  require_once "crud.class.php";

  // Our Controller class
  require_once "app.class.php";

  // Start App
  $app = new App();
  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $input = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $result = $app->postRequest($input);
  } else {
    $input = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    $result = $app->getRequest($input);
  }

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gerenciamento de Cobrança">
    <meta name="author" content="samuelxavi@gmail.com">
    <link rel="icon" href="favicon.ico">

    <title>Receiv | Gerenciamento de Cobrança</title>

    <link rel="canonical" href="http://localhost/teste-receiv/">

    <!-- Custom styles for this template -->
    <link href="assets/css/styles.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Sweet Alerts -->
    <link href="assets/css/sweetalert.css" rel="stylesheet">

  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Receiv</a>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="<?php echo HOME_URL; ?>">
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>
            </ul>
            
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <?php if(!empty($app->success_message)): ?>
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?php echo $app->success_message; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php elseif(!empty($app->error_message)): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?php echo $app->error_message; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php endif; ?>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
            <h1 class="h2">Gerenciamento de cobrança</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button id="btn-newregister" class="btn btn-sm btn-outline-secondary">Novo cadastro</button>
              </div>
            </div>
          </div>     
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th></th>
                  <th>Nome</th>
                  <th>CPF/CNPJ</th>
                  <th>Data de nascimento</th>
                  <th>Endereço</th>
                  <th>Valor</th>
                  <th>Data de vencimento</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($result as $row): ?>
                <tr>
                  <td>
                    <button type="button" class="btn btn-info btn-sm edit-invoice" data-invoice="<?php echo $row['id'] ?>">Editar</button>
                    <button type="button" class="btn btn-danger btn-sm delete-invoice" data-invoice="<?php echo $row['id'] ?>">Excluir</button>
                  </td>
                  <td><?php echo $row['name'] ?></td>
                  <td><?php echo $row['document'] ?></td>
                  <td><?php echo $row['birthdate'] ?></td>
                  <td><?php echo $row['address'] ?></td>
                  <td><?php echo $row['amount'] ?></td>
                  <td><?php echo $row['duedate'] ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="novoCadastro" tabindex="-1" role="dialog" aria-labelledby="cadastroLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form id="form-invoice" method="post">
            <div class="modal-header">
              <h4 class="modal-title" id="cadastroLabel">Cadastro de cobrança</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <fieldset>
              <input name="id" type="hidden" id="id_invoice">

              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="name">Nome</label>  
                <input name="name" type="text" id="name" class="form-control input-md" required>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="document">CPF / CNPJ</label>  
                <input name="document" type="text" id="document" placeholder="" class="form-control input-md" required>
              </div>


              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="birthdate">Data de nascimento</label>  
                <input name="birthdate" type="date" id="birthdate" class="form-control input-md" required>
              </div>


              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="address">Endereço</label>  
                <input name="address" type="text" id="address" class="form-control input-md" required>
              </div>


              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="birthdate">Descrição do título</label>  
                <textarea class="form-control" name="description" id="description" required></textarea>
              </div>


              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="amount">Valor</label>  
                <input name="amount" type="text" id="amount" class="form-control input-md" required>
              </div>


              <!-- Text input-->
              <div class="form-group">
                <label class="control-label" for="duedate">Vencimento</label>  
                <input name="duedate" type="date" id="duedate" class="form-control input-md" required>
              </div>

              </fieldset>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info">Salvar</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Bootstrap core and Plugins JavaScript
    ================================================== -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.mask.min.js"></script>
    <script src="assets/js/sweetalert.min.js"></script>
    
    <!-- App Custom Scripts -->
    <script src="assets/js/app.js"></script>

  </body>
</html>