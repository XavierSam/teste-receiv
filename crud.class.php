<?php

/**
* Database Class
* 
* @author samuelxavi@gmail.com
* 
*/
class Crud {

    private $conn = null;
    public $error = null;


    /**
     * Faz a conexão com o DB
     * Popula parâmetros iniciais da classe
     * 
     * @param void
     * @return void
     */
    public function __construct() {
        try {
            $str = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME;
            $this->conn = new PDO($str, DB_USER, DB_PASS);
            return true;
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }
    }


     /**
     * Atualiza um registro na tabela informada de acordo com ID
     * Retorna número de registros afetados
     * 
     * @param string $table
     * @param array $data
     * @param int $id
     * @return int
     */
    public function update($table, $data, $id) {

        $params = [];
        $fields = [];
        foreach ($data as $key => $value) {
            $fields[] = "$key = :$key";
            $params[":$key"] = $value;
        }
        $fields = implode(", ", $fields);
        $sql = "UPDATE $table SET $fields WHERE id = :id";
        $params[":id"] = $id;

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute($params);
            return $stmt->rowCount();
        }
        catch (Exception $ex) {
            return 0;
        }
    }

     /**
     * Insere um registro na tabela informada
     * Retorna número de registros afetados
     * 
     * @param string $table
     * @param array $data
     * @return int
     */
    public function insert($table, $data) {
        $params = [];
        $fields = [];
        $values = [];

        foreach ($data as $key => $value) {
            $fields[] = "$key";
            $values[] = ":$key";
            $params[":$key"] = $value;
        }
        $fields = implode(", ", $fields);
        $values = implode(", ", $values);

        $sql = "INSERT INTO $table ($fields) VALUES ($values)";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute($params);
            return $stmt->rowCount();
        }
        catch (Exception $ex) {
            return 0;
        }
    }

     /**
     * Remove um registro na tabela informada de acordo com id
     * Retorna número de registros afetados
     * 
     * @param string $table
     * @param array $data
     * @param int $id
     * @return int
     */
    public function delete($table, $id) {

        $sql = "DELETE FROM $table WHERE id = :id";
        $params[":id"] = $id;
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute($params);
            return $stmt->rowCount();
        }
        catch (Exception $ex) {
            return 0;
        }
    }

    
     /**
     * Busca todos os registros da tabela informada
     * 
     * @param string $table
     * @return array|bool
     */
    function fetchAll($table) {
        $sql = "SELECT * from $table";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();
        }
        catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return false;
        }
    }


     /**
     * Busca um registro da tabela informada de acordo com id
     * 
     * @param string $table
     * @param int $id
     * @return array|bool
     */
    function fetchRow($table, $id) {
        $sql = "SELECT * from $table WHERE id = ?";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(array($id));

            return $stmt->fetch();
        }
        catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return false;
        }
    }
} 
// End Crud class

?>