
## Teste para processo seletivo Receiv

Essa aplicação foi desenvolvida para o processo seletivo Receiv conforme  descrição: "Desenvolver um CRUD em PHP puro (orientado a objetos e sem utilizar framework) atendendo à especificação abaixo."

1. Monte uma base de cadastro de devedores e dívidas com, no mínimo, a seguinte estrutura (modelagem de dados importa):
	Nome:   string
	CPF/CNPJ:   integer   
	Data de nascimento:   date
	Endereço: string
	Descrição do título: text
	valor:   float
	Data de vencimento:   date
	updated:   datetime
	Utilize MySQL.
2. A aplicação deve ser cross browser support (chrome, firefox, etc)
3. Dashboard com indicadores que julgue apropriados (Bônus star)
4. Escrever teste unitário (não obrigatório, mas ganha pontos extras)

A aplicação está publicada em: https://testereceiv.xavier.dev.br/

---

### Pré-requisitos

 - Apache
 - PHP >= 7.2
 - MySQL >= 5.6
 - PHPMyAdmin ou qualquer cliente de MySql
 
---

### Estrutura

 - **config.php** - Arquivo de configuração de URL e credenciais de acesso.
 - **app.class.php** - Controller principal da aplicação
 - **crud.class.php** - Classe de gerenciamento de comunicação com DB.
 - **index.php** - Página inicial com todo o markup da aplicação
 - **db_structrure.sql** - Arquivo com a estrutura do banco de dados.

---

### Instalação

 1. Clonar o repositório no diretório web do apache.
 2. Criar um banco de dados no mysql.
 3. Importar o arquivo db_structrure.sql para o banco de dados criado
 4. informar no arquivo config.php as credenciais de acesso e a URL do ambiente.
 
---

### Autor

Samuel Xavier Silva (samuelxavi@gmail.com)